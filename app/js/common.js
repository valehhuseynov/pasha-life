/*
   From here you can add custom js.
   -----
*/

// Header Navbar Language

   $(".Header-navbar__langlink").on("click", () => {
      $(".Header-navbar__content").toggle();
   });

   $(".Header-navbar__contentlink").on("click", function(e) {
      let lang;
      let href;
      if(e.target.className === "Header-navbar__contentlink") {
         lang = $(e.target).text();
         href = $(e.target).attr("href");
         console.log(href);
      }
      
      $(".Header-navbar__activelang").text(lang);
      $(".Headetr-navbar__langlink").attr("href" , href);
      $(".Header-navbar__content").hide();
   });

// End Header Navbar Language

// Header Mobile Nav Bar
$(".Header-mobile__langlink").on("click", () => {
   $(".Header-mobile__content").toggle();
});

$(".Header-mobile__contentlink").on("click", (e) => {
   let lang;
   let href;
   if(e.target.className === "Header-mobile__contentlink") {
      lang = $(e.target).text();
      href = $(e.target).attr("href");
      console.log(href);
   }
   
   $(".Header-mobile__activelang").text(lang);
   $(".Headetr-mobile__langlink").attr("href" , href);
   $(".Header-mobile__content").hide();
});

$(".Header-mobile__navbutton").on("click", function() {
   $(".Header-mobile__menu").toggle();
});
$(".Header-mobile__close , .Header-mobile__link").on("click", function() {
   $(".Header-mobile__menu").toggle();
});

$(".Header-mobile__link").on("click", function() {
   $(".Header-mobile__link").removeClass("linkactive");
   $(this).addClass("linkactive");
})
   
// End Header Mobile Nav Bar
// Calculator Tab

   function openContent(e , contenName) {

      let index, tabcontent, tablinks;

      tabcontent = document.getElementsByClassName("Calculator-tab__content");
      for ( index = 0; index < tabcontent.length; index++) {
         tabcontent[index].style.display = "none";
      }

      tablinks = document.getElementsByClassName("Calculator-tab__link");
      for ( index = 0; index < tablinks.length; index++) {
         tablinks[index].className = tablinks[index].className.replace("btnactive", "");
      }

      document.getElementById(contenName).style.display = "block";
      e.target.className += " btnactive";
   }

// End Calculator Tab

// Faq
   $(".card-header").on("click", function(e){
      if($(this).hasClass("collapsed")) {
         $(this).children().children().children().attr("src", "../../img/icons/subtract.png");
      } else {
         $(this).children().children().children().attr("src", "../../img/icons/plus_2.png");
      }
   });
   
// End Faq

$("#Header-navbar__about").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#about").offset().top},1000);
});
$("#Header-navbar__calculator").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#calculator").offset().top},1000);
});
$("#Header-navbar__faq").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#faq").offset().top},1000);
});
$("#Header-navbar__order").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#order").offset().top},1000);
});
$("#Header-navbar__contact").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#contact").offset().top},1000);
});

$(".Header__mouse img").click(function(e) {
   $('html,body').animate({
       scrollTop: $("#advantages").offset().top},1000);
});


// $(".Subscribe-form__subbutton").on("click", function(e) {
//    e.preventDefault();
// });
// $(".Contact-input__send").on("click", function(e) {
//    e.preventDefault();
// });

// Video 
   var video = document.getElementById("video");
   var juice = document.getElementById("green-juice");
   var bar = document.getElementById("green-bar");
   var btn = document.getElementById("play-pause");
   var button = document.getElementById("play-button");
   var play = document.getElementById("play-image");
   var pause = document.getElementById("pause-image");
   var controls = document.getElementById("controls");

   function togglePlayPouse() {
      if(video.paused) {
         btn.className = "pause";
         video.play();
         play.style.display = "none";
         pause.style.display = "block";
      } else {
         btn.className = "play";
         video.pause();
         pause.style.display = "none";
      }
   }

   btn.addEventListener("click", function() {
      togglePlayPouse();
   });
   video.addEventListener("timeupdate", function() {
      var juicePos = video.currentTime / video.duration;
      juice.style.width = juicePos * 100 + "%";
      juice.style.backgroundColor = "#1bbc9d";
      bar.style.background = "#000";
   })
   button.addEventListener("mouseover", function() {
      if(btn.className === "pause" ){
         pause.style.display = "block";
      } else {
         pause.style.display = "none";
         play.style.display = "block";
      }
   });
   button.addEventListener("mouseout", function() {
      if(btn.className === "play") {
         pause.style.display = "none";
         play.style.display = "block";
      } else {
         pause.style.display = "block";
         pause.style.display = "none";
      }
   });
   // controls.addEventListener("click",function() {
   //    btn.style.display = "block";
      
   // });
   
// End Video